// alert("Hello Batch 295!");


// [SECTION] Functions in JS are lines/blocks of code that tell our device/application to perform a certain task when it is called/invoked.
// They are also used to prevent repeating lines/blocks of codes that perform the same function/task.


// Function Declarations
	// function statement defines a function with specified parameters.
	// functionName - the function name. This is the name we call when we invoke our function
	// function code block({}) - statement/ body of the function. This is where we find the code to be executed.


	/*
		Syntax:
			function functionName() {
				code block(statement)
			}


	*/

	function printName() {
		console.log("Hi! My name is john.");
	};

	// Function invocation
		// The code block statements inside a function is not immediately executed when the function is defined. The code block and statement inside the function is executed only when the function is called or invoked.



	// printName() calls the function we define above.
	printName();


	// Function Declaration vs Function Expression

		// Function Declaration
		declaredFunction(); //declared function can be hoisted

		// Hoisting in JavaScript is for certain variables and functions to run or use them before their declaration.

		function declaredFunction() {
			console.log("Hello World from declaredFunction()");


		};

		declaredFunction();

		// Function Expression
			// a function that is stored in a variable.
			// a function expression is an anonymous function assigned to a variable.
				// anonymous functon - a function without a name.
		// variableFunction(); - function expression cannot be hoisted

			
			let variableFunction = function(){
				console.log("Hello again, Batch 295!")
			}

			variableFunction();

			// We can also create a function expression of a named function. 
			// However, to invoke the function, we still call it by its variable name.

			let funcExpression = function funcName(){
				console.log("Hello from the other side");
			}

			// funcName(); - will result to a ref error
			funcExpression(); // we call the variable name.

			// Reassigning functions
				// can only be done with function expression.

			declaredFunction = function(){
				console.log("updated declaredFunction")
			}
			declaredFunction();
			declaredFunction();

			funcExpression= function(){
				console.log("updated funcExpression");
			}
			funcExpression();

			// Reassigning Constant Function Expression

			const constantFunc = function(){
				console.log("This is initialized with const!");
			}

/*			constantFunc();
			constantFunc = function(){
				console.log("Cannot be reassigned");
			};

			constantFunc();

			
				-this will result into a type error
*/			

	//function scope


		/* 
			Scope is the accessibility (visibility) of variables within a program.

			JavaScript Variables has 3 types of scopes:
				1. local/block scope -within the code block only.
				2. global scope - variable can be accessed anywhere in our program/file
				3. function scope - variable can be access within the same function only.



		*/
			//local scope 
			{
				let localVar = "John Smith";
				console.log(localVar);
			}
			//global scope
			let globalVar ="Mr. Worldwide";

			console.log(globalVar);
			// console.log(localVar); //will result to an error.

			// function scope
			function showNames(){
				// function scoped variables
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet ="Jane";
				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);
				
			}


			showNames();
			// all these will result to an error since the variables has function scope.
			// console.log(functionVar);
			// console.log(functionConst);
			// console.log(functionLet);


		// Nested functions
			// Function within a function.


			function myNewFunction(){
				let name="Jane";

				function nestedFunction(){
					let nestedName = "John";
					console.log(name);
				}
				// console.log(nestedName); //will result to an error.
				nestedFunction()
			}

			myNewFunction();
			// nestedFunction() - will result to an error.

		// Function and Global Scope Variables

			// global variable
			let globalName ="Alejandro";

			function anotherFunction(){

				let nameInside = "Cassandra";
				console.log(globalName);
			}

			anotherFunction();

			function anotherFunction2() {
				globalName ="Alexander"
				console.log(globalName);
			}

			anotherFunction2();
			// The new name is passed on since JS's behavior is synchronous.
			console.log(globalName);



	// The Return Statement
		/*
			The "return" statement allow us to output a value from a function in to the browser. It passes the data from the function code block to either a form element/ table element/ component in our frontend application.

		*/

	function returnFullName(){
		return "Jeffrey" + " " + "Smith" + " " + "Bezos";
		console.log("This message will not be printed.");
	}

	let fullName = returnFullName();
	console.log(fullName);

	// In this example, console.log() will print the returned value of the returnFullName() function.
	console.log(returnFullName());


	function returnFullAddress(){

		let fullAddress = {
			street: "#44 Maharlika St.",
			city: "Cainta",
			province: "Rizal"
		};

		return fullAddress;

	}

	let myAddress = returnFullAddress();
	console.log(myAddress);

	function printPlayerInfo(){
		console.log("Username: " + "white_knight");
		console.log("Level: " + 95);
		console.log("Job: " + "paladin");

		// return "Username: white_knight"; - without a return statement the function will result to undefined.
	}

	let user1 = printPlayerInfo();
	console.log(user1);



	// Function Naming Convention
		// function names should be definitive of the task it will perform.
		// function names usually contains a verb or an action word.
		// In most cases we use camelCasing in writing the functionName.


	function getCourses(){
		let courses = ["Science 202", "Math 304", "English 101"];
		return courses;

	}

	let courses = getCourses();
	console.log(courses);

	// Avoid generic function names to avoid confusion within our code.

	function get(){
		let name = "Jamie";
		return name;
	}

	let name = get ();
	console.log(name);

	// Avoid pointless and inappropriate function name.

	function foo(){
		return 25%5;
	}

	console.log(foo());